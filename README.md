> **IMPORTANT** - This contract has not yet undergone a third-party security audit, See LICENSE.txt for usage details

# Algorando

> An on-chain randomness oracle for Algorand using SHA256_512

## Deployments

> All developers **must** use the same instance of Algorando to ensure maximum entropy. You do **not** need to deploy your own instance of Algorando

 - **Testnet**
     - App ID - 57896035
     - Create Txn: [ETARPIVCTBWQCT7CXOXCBNPC7LDL67N6RQMB4LLBRSZVBPFHORNQ](https://testnet.algoexplorer.io/tx/ETARPIVCTBWQCT7CXOXCBNPC7LDL67N6RQMB4LLBRSZVBPFHORNQ)

## How-to-use

1. Create an atomic transaction containing 2 (or more) Transactions
1. The first transaction will invoke the on-chain algorando deployment (See above for appId)
    - The transaction can provide a single argument, an encoded uint64 describing how many 32byte slices of randomness you would like generated
1. The second transaction will invoke your on-chain application where it can access the random value in the 0th scratch slot
    - The value will be a byte[]
    - the length of the byte[] will be: `appArg[0] * 32`

# Examples

## Creating grouped transaction to Algorando (JS)

```typescript
const suggestedParams = await this.algod.getTransactionParams().do(); // Create your own instance of algod
const algorandoTx = algosdk.makeApplicationNoOpTxnFromObject({
    from: account,
    appIndex: <algorando_appId>,
    suggestedParams,
    appArgs: [
        algosdk.encodeUint64(2) // Results in (32 * 2) bytes of randomness
    ]
});

const appTx = algosdk.makeApplicationNoOpTxnFromObject({
    from: account,
    appIndex: appIndex,
    suggestedParams,
});

const txGroupToBeSigned = algosdk.assignGroupID([algorandoTx, appTx]);
```

## Reading the random bytes from scratch (PyTEAL)

```python
# n = Whichever transaction in the group that made the transaction to Algorando

Assert(Gtxn[<n>].application_id() == <algorando_appId>)
randomBytes = ImportScratchValue(<n>, 0)

# Create a random number (PyTeal >0.9)
randomInt = Btoi(Extract(randomBytes, Int(0), Int(8)))
```

> Further examples can be found by looking at unit tests

## Pitfalls
- **Pitfall**: Users could create an Atomic Transfer that contains a call to an impersonator app that is not random
   - > Mitigation: Developers **must** assert the application_id of the transaction to ensure the transaction group doesn't contain a call to a different, unexpected application that will impersonate Algorando

## Risks


 - **Risk**: Users could brute-force a sender address that could produce a favorable value, compromising the 'randomness'
   - > Mitigation: The last block time is unpredictable and changes every block, about ~4.5s. The last block time is incorporated when calculating the random value (See, Global.latest_timestamp()). This puts a deadline on any would be abusers
   - > Mitigation: Developers **should** incorporate a call to Algorando into their application's transactions even when a random value is not needed.  This creates additional entropy in each block. Because an attacker can't guarantee the order transactions are evaluated in a block, they have less chance of creating a favorable sender address and being in a block that doesn't contribute additional entropy to Algorando.
   - > Mitigation: The more developers and dapps using Algorando, the more transactions will be made to Algorando creating more entropy.  All developers **must** use the same instance of Algorando to ensure maximum entropy

## References

 - [Atomic Transfers](https://developer.algorand.org/docs/get-details/atomic_transfers/)
 - [Scratch Space](https://developer.algorand.org/docs/get-details/dapps/avm/teal/specification/#scratch-space)
 - [PyTeal: Scratch Space](https://pyteal.readthedocs.io/en/stable/scratch.html)
 - [PyTeal: Loading Scratch Slots from transaction group](https://pyteal.readthedocs.io/en/stable/loading_group_transaction.html#loading-scratch-slots)

## Coming Up
 - The Nonce needs to be mod (%) by the MAX_INT value after incrementing to ensure it doesnt overflow in the future
 - Hashing algorithm will be configurable by a second app_arg
 - Looking for funding to perform a security audit
 - Drop an [issue](https://gitlab.com/zcircuit/algorando/-/issues) on the repo for any further requests