const { getProgram } = require('@algo-builder/runtime');
const { types: wtypes } = require('@algo-builder/web');

const FEE = 1e3;
const algorandoProgram = getProgram('algorando.py')
const testProgram = getProgram('getAlgorandoBytes.py')
const clearProgram = getProgram('noop-clear.teal');

function nextRandomValueFactory(runtime, master) {

    const algorando = runtime.deployApp(algorandoProgram, clearProgram, {
        sender: master.account,
        globalBytes: 1,
        globalInts: 1,
    }, {});

    const testApp = runtime.deployApp(testProgram, clearProgram, {
        sender: master.account,
        globalBytes: 8,
        globalInts: 1,
    }, {});

    return [(altSenderAddress, numShas = 8) => {
        runtime.executeTx([
            {
                type: wtypes.TransactionType.CallApp,
                sign: wtypes.SignType.SecretKey,
                fromAccountAddr: altSenderAddress || master.address,
                appID: algorando.appID,
                payFlags: {
                    totalFee: FEE,
                },
                appArgs: [
                    // algosdk.encodeUint64(8)
                    `int:${numShas}`,
                ]
            },
            {
                type: wtypes.TransactionType.CallApp,
                sign: wtypes.SignType.SecretKey,
                fromAccountAddr: master.address,
                appID: testApp.appID,
                payFlags: {
                    totalFee: FEE,
                },
            }
        ]);

        return Array.from({ length: numShas })
            .flatMap((_, i) => Array.from(runtime.getGlobalState(testApp.appID, `value${i + 1}`)))
    }, algorando.appID, testApp.appID]
}

module.exports = {
    nextRandomValueFactory,
}