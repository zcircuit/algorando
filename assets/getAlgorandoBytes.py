# Do not deploy, just used for tests against Algorando

from pyteal import (App, Approve, Bytes, Global, ImportScratchValue, Mode, Seq,
                    compileTeal, Substring, Int, If, Txn)


def getAlgorandoBytes():
    return Seq([
        If(Txn.application_id() == Int(0)).Then(Approve()),
        App.globalPut(Bytes('value1'), Substring(
            ImportScratchValue(0, 0), Int(32 * 0), Int(32 * 1))),
        App.globalPut(Bytes('value2'), Substring(
            ImportScratchValue(0, 0), Int(32 * 1), Int(32 * 2))),
        App.globalPut(Bytes('value3'), Substring(
            ImportScratchValue(0, 0), Int(32 * 2), Int(32 * 3))),
        App.globalPut(Bytes('value4'), Substring(
            ImportScratchValue(0, 0), Int(32 * 3), Int(32 * 4))),
        App.globalPut(Bytes('value5'), Substring(
            ImportScratchValue(0, 0), Int(32 * 4), Int(32 * 5))),
        App.globalPut(Bytes('value6'), Substring(
            ImportScratchValue(0, 0), Int(32 * 5), Int(32 * 6))),
        App.globalPut(Bytes('value7'), Substring(
            ImportScratchValue(0, 0), Int(32 * 6), Int(32 * 7))),
        App.globalPut(Bytes('value8'), Substring(
            ImportScratchValue(0, 0), Int(32 * 7), Int(32 * 8))),
        App.globalPut(Bytes('now'), Global.latest_timestamp()),
        Approve(),
    ])


if __name__ == "__main__":
    print(compileTeal(getAlgorandoBytes(), mode=Mode.Application, version=5))
